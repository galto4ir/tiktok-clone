// Import the functions you need from the SDKs you need
import { getStorage } from "@firebase/storage";
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDJUXlUAwyzIATR0cZNemb1Tt32Lna46tM",
  authDomain: "tiktok-96152.firebaseapp.com",
  projectId: "tiktok-96152",
  storageBucket: "tiktok-96152.appspot.com",
  messagingSenderId: "275122081479",
  appId: "1:275122081479:web:4125d9d7c82018737ff8e2",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const storage = getStorage(app);
export { app, auth, db, storage };
