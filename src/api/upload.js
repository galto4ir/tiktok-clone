import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { storage, auth, db } from "../lib/firebase";

import { collection, addDoc } from "firebase/firestore";

const apiUpload = {
  post: async (title, desc, videoFile) => {
    try {
      const currentUser = auth.currentUser;
      const storageRef = ref(
        storage,
        `/posts/${new Date().getTime()}.${videoFile.name.split(".").pop()}`
      );

      // 'file' comes from the Blob or File API
      uploadBytes(storageRef, videoFile).then((snapshot) => {
        getDownloadURL(storageRef).then(async (url) => {
          await addDoc(collection(db, "posts"), {
            title,
            desc,
            uid: currentUser.uid,
            videoURL: url,
          });
        });
      });
    } catch (e) {
      console.log("ERROR: ", e);
    }
  },
};

export default apiUpload;
