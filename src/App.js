import { Routes, Route } from "react-router-dom";
import { ProvideAuth } from "./hooks/useAuth";
import Home from "./pages/Home";
import SignUp from "./pages/SignUp";
import SignIn from "./pages/SignIn";
import UploadVideo from "./pages/UploadVideo";
function App() {
  return (
    <ProvideAuth>
      <Routes>
        <Route path="/signin" element={<SignIn />} />
        <Route path="/upload" element={<UploadVideo />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/" element={<Home />} />

        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Routes>
    </ProvideAuth>
  );
}

export default App;
