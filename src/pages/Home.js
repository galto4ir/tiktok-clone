// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.min.css";
import Video from "../components/Video";

export default () => {
  return (
    <Swiper
      effect={"cube"}
      grabCursor={true}
      spaceBetween={0}
      slidesPerView={1}
      onSlideChange={() => console.log("slide change")}
      onSwiper={(swiper) => console.log(swiper)}
      direction="vertical"
    >
      <SwiperSlide>
        <Video videoURL={"/example3.mp4"} />
      </SwiperSlide>
      <SwiperSlide>
        <Video videoURL={"/example2.mp4"} />
      </SwiperSlide>
      <SwiperSlide>
        <Video videoURL={"/example.mp4"} />
      </SwiperSlide>
    </Swiper>
  );
};
