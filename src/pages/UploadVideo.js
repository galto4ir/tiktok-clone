import * as React from "react";
import apiUpload from "../api/upload";

const UploadVideo = (props) => {
  const [video, setVideo] = React.useState(null);
  const upload = async () => {
    if (video) {
      await apiUpload.post("Test", "Test", video);
    }
  };
  return (
    <section className="flex flex-col p-10 container mx-auto">
      <h1>Upload Video</h1>
      <input type="text" placeholder="Title" className="border p-2 mb-5" />
      <textarea
        rows={10}
        cols={20}
        placeholder="Description"
        className="border p-2 mb-5"
      />
      <input
        type="file"
        accept="video/*"
        placeholder="Video File"
        onChange={(e) => {
          const [file] = e.target.files;
          setVideo(file);
        }}
      />
      <button className="border border-blue-500 rounded p-3" onClick={upload}>
        upload video
      </button>
    </section>
  );
};

export default UploadVideo;
