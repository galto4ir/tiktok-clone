import { db } from "../lib/firebase";
import { setDoc, doc } from "firebase/firestore";
const apiUsers = {
  init: async (uid, email, firstName, lastName) => {
    try {
      await setDoc(doc(db, "users", uid), {
        email,
        firstName,
        lastName,
      });
      return { success: true };
    } catch (e) {
      return { success: false };
    }
  },
};
export default apiUsers;
