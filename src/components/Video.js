import * as React from "react";

const Video = ({ videoURL }) => {
  return (
    <section className="h-screen relative w-full  bg-green-500 text-white flex items-end">
      <video
        src={videoURL}
        controls={false}
        className="absolute w-full object-fit h-full top-0 left-0 right-0 bottom-0"
      ></video>

      <div className="absolute bg-black bg-opacity-50 top-0 left-0 right-0 bottom-0" />

      <div className="flex items-end relative w-full p-5">
        <div className="w-full">
          <h1 className="font-bold text-lg">@galt</h1>
          <p className="text-sm">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </p>
        </div>
        <div className="flex flex-col items-center space-y-5">
          <div className="w-12 h-12 relative rounded-full bg-white">
            <div className="absolute -bottom-2 left-1/2 transform -translate-x-1/2 w-5 h-5 bg-red-400 flex items-center justify-center rounded-full">
              <span className="mi material-icons-outlined sm-14">add</span>
            </div>
          </div>
          <section className="flex items-center flex-col">
            <span className="mi material-icons-outlined">favorite_border</span>
            <span className="text-sm">39.8k</span>
          </section>
          <section className="flex items-center flex-col">
            <span className="mi material-icons-outlined">textsms</span>
            <span className="text-sm">8.6k</span>
          </section>

          <section className="flex items-center flex-col">
            <span className="mi material-icons-outlined">share</span>
            <span className="text-sm">Share</span>
          </section>
        </div>
      </div>
    </section>
  );
};

export default Video;
